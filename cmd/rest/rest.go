/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package rest

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/spf13/cobra"
	"go.uber.org/zap"

	"bitbucket.com/efishery/gourami/config"
	"bitbucket.com/efishery/gourami/internal/customersvc"
	"bitbucket.com/efishery/gourami/internal/postgresql"
	"bitbucket.com/efishery/gourami/internal/productsvc"
	"bitbucket.com/efishery/gourami/internal/rest"
	"bitbucket.com/efishery/gourami/internal/services"
	xmiddleware "bitbucket.com/efishery/gourami/pkg/middleware"
	"bitbucket.com/efishery/gourami/pkg/storage/xgorm"
	"bitbucket.com/efishery/gourami/pkg/storage/xvault"
	"bitbucket.com/efishery/gourami/pkg/xerrors"
)

var (
	ENV     string
	Address string
)

// RestCmd represents the rest command.
var RestCmd = &cobra.Command{
	Use:   "rest",
	Short: "rest api for supply service",
	Run: func(cmd *cobra.Command, args []string) {
		logger, err := zap.NewProduction()
		if err != nil {
			panic(xerrors.WrapErrorf(err, xerrors.ErrorCodeUnknown, "zap.NewProduction"))
		}

		route := chi.NewRouter()

		// // Base middleware
		route.Use(middleware.RequestID)
		route.Use(middleware.RealIP)
		route.Use(xmiddleware.Logger(logger))
		route.Use(middleware.Recoverer)

		// Load configuration
		_ = config.Load(".env.example")
		vault, err := xvault.NewVaultProvider()
		if err != nil {
			panic(xerrors.WrapErrorf(err, xerrors.ErrorCodeUnknown, "internal.NewVaultProvider"))
		}
		conf := config.New(vault)

		// Init Repository
		pool, err := xgorm.NewGorm(conf)
		if err != nil {
			panic(err)
		}
		repos := postgresql.NewSupplyCanvassing(pool)

		customerServiceURL := os.Getenv("CUSTOMER_SERVICE_URL")
		customerServiceToken := os.Getenv("CUSTOMER_SERVICE_TOKEN")
		csSvc := customersvc.NewCustomerService(customerServiceURL, customerServiceToken)

		productServiceURL := os.Getenv("CUSTOMER_SERVICE_URL")
		productServiceToken := os.Getenv("CUSTOMER_SERVICE_TOKEN")
		productSvc := productsvc.NewProductService(productServiceURL, productServiceToken)

		svc := services.NewSupply(repos, csSvc, productSvc)
		rest.NewSupplyHandler(svc).Register(route)

		srv := &http.Server{
			Handler:           route,
			Addr:              fmt.Sprintf(":%s", Address),
			ReadTimeout:       1 * time.Second,
			ReadHeaderTimeout: 1 * time.Second,
			WriteTimeout:      1 * time.Second,
			IdleTimeout:       1 * time.Second,
		}

		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

		go func() {
			logger.Info("Listening on port :", zap.String("address", Address))
			if err := srv.ListenAndServe(); err != nil {
				if errors.Is(err, http.ErrServerClosed) {
					log.Fatal(err)
				}
			}
		}()

		sig := <-c
		logger.Info("shutting down: %+v", zap.Any("signal", sig))

		ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
		defer cancel()

		if err := srv.Shutdown(ctx); err != nil {
			logger.Fatal("error shutingdown server", zap.Error(err))
		}

	},
}
