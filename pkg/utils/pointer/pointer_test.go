package pointer

import "testing"

func TestInt32(t *testing.T) {
	val := int32(0)
	ptr := Int32(val)
	if *ptr != val {
		t.Errorf("expected %d, got %d", val, *ptr)
	}

	val = int32(1)
	ptr = Int32(val)
	if *ptr != val {
		t.Errorf("expected %d, got %d", val, *ptr)
	}
}

func TestInt32Deref(t *testing.T) {
	var val, def int32 = 1, 0

	out := Int32Deref(&val, def)
	if out != val {
		t.Errorf("expected %d, got %d", val, out)
	}

	out = Int32Deref(nil, def)
	if out != def {
		t.Errorf("expected %d, got %d", def, out)
	}
}

func TestInt32Equal(t *testing.T) {
	if !Int32Equal(nil, nil) {
		t.Errorf("expected true (nil == nil)")
	}
	if !Int32Equal(Int32(123), Int32(123)) {
		t.Errorf("expected true (val == val)")
	}
	if Int32Equal(nil, Int32(123)) {
		t.Errorf("expected false (nil != val)")
	}
	if Int32Equal(Int32(123), nil) {
		t.Errorf("expected false (val != nil)")
	}
	if Int32Equal(Int32(123), Int32(456)) {
		t.Errorf("expected false (val != val)")
	}
}

func TestInt64(t *testing.T) {
	val := int64(0)
	ptr := Int64(val)
	if *ptr != val {
		t.Errorf("expected %d, got %d", val, *ptr)
	}

	val = int64(1)
	ptr = Int64(val)
	if *ptr != val {
		t.Errorf("expected %d, got %d", val, *ptr)
	}
}

func TestInt64Deref(t *testing.T) {
	var val, def int64 = 1, 0

	out := Int64Deref(&val, def)
	if out != val {
		t.Errorf("expected %d, got %d", val, out)
	}

	out = Int64Deref(nil, def)
	if out != def {
		t.Errorf("expected %d, got %d", def, out)
	}
}

func TestInt64Equal(t *testing.T) {
	if !Int64Equal(nil, nil) {
		t.Errorf("expected true (nil == nil)")
	}
	if !Int64Equal(Int64(123), Int64(123)) {
		t.Errorf("expected true (val == val)")
	}
	if Int64Equal(nil, Int64(123)) {
		t.Errorf("expected false (nil != val)")
	}
	if Int64Equal(Int64(123), nil) {
		t.Errorf("expected false (val != nil)")
	}
	if Int64Equal(Int64(123), Int64(456)) {
		t.Errorf("expected false (val != val)")
	}
}

func TestBool(t *testing.T) {
	val := false
	ptr := Bool(val)
	if *ptr != val {
		t.Errorf("expected %t, got %t", val, *ptr)
	}

	val = true
	ptr = Bool(true)
	if *ptr != val {
		t.Errorf("expected %t, got %t", val, *ptr)
	}
}

func TestBoolDeref(t *testing.T) {
	val, def := true, false

	out := BoolDeref(&val, def)
	if out != val {
		t.Errorf("expected %t, got %t", val, out)
	}

	out = BoolDeref(nil, def)
	if out != def {
		t.Errorf("expected %t, got %t", def, out)
	}
}

func TestBoolEqual(t *testing.T) {
	if !BoolEqual(nil, nil) {
		t.Errorf("expected true (nil == nil)")
	}
	if !BoolEqual(Bool(true), Bool(true)) {
		t.Errorf("expected true (val == val)")
	}
	if BoolEqual(nil, Bool(true)) {
		t.Errorf("expected false (nil != val)")
	}
	if BoolEqual(Bool(true), nil) {
		t.Errorf("expected false (val != nil)")
	}
	if BoolEqual(Bool(true), Bool(false)) {
		t.Errorf("expected false (val != val)")
	}
}

func TestString(t *testing.T) {
	val := ""
	ptr := String(val)
	if *ptr != val {
		t.Errorf("expected %s, got %s", val, *ptr)
	}

	val = "a"
	ptr = String(val)
	if *ptr != val {
		t.Errorf("expected %s, got %s", val, *ptr)
	}
}

func TestStringDeref(t *testing.T) {
	val, def := "a", ""

	out := StringDeref(&val, def)
	if out != val {
		t.Errorf("expected %s, got %s", val, out)
	}

	out = StringDeref(nil, def)
	if out != def {
		t.Errorf("expected %s, got %s", def, out)
	}
}

func TestStringEqual(t *testing.T) {
	if !StringEqual(nil, nil) {
		t.Errorf("expected true (nil == nil)")
	}
	if !StringEqual(String("abc"), String("abc")) {
		t.Errorf("expected true (val == val)")
	}
	if StringEqual(nil, String("abc")) {
		t.Errorf("expected false (nil != val)")
	}
	if StringEqual(String("abc"), nil) {
		t.Errorf("expected false (val != nil)")
	}
	if StringEqual(String("abc"), String("def")) {
		t.Errorf("expected false (val != val)")
	}
}
