package pointer

// Int32 returns a pointer to an int32.
func Int32(i int32) *int32 {
	return &i
}

var Int32Ptr = Int32 // for back-compat

// Int32Deref dereferences the int32 ptr and returns it if not nil, or else
// returns def.
func Int32Deref(ptr *int32, def int32) int32 {
	if ptr != nil {
		return *ptr
	}

	return def
}

var Int32PtrDerefOr = Int32Deref // for back-compat

// Int32Equal returns true if both arguments are nil or both arguments
// dereference to the same value.
func Int32Equal(a, b *int32) bool {
	if (a == nil) != (b == nil) {
		return false
	}
	if a == nil {
		return true
	}

	return *a == *b
}

// Int64 returns a pointer to an int64.
func Int64(i int64) *int64 {
	return &i
}

var Int64Ptr = Int64 // for back-compat

// Int64Deref dereferences the int64 ptr and returns it if not nil, or else
// returns def.
func Int64Deref(ptr *int64, def int64) int64 {
	if ptr != nil {
		return *ptr
	}

	return def
}

var Int64PtrDerefOr = Int64Deref // for back-compat

// Int64Equal returns true if both arguments are nil or both arguments
// dereference to the same value.
func Int64Equal(a, b *int64) bool {
	if (a == nil) != (b == nil) {
		return false
	}
	if a == nil {
		return true
	}

	return *a == *b
}

// Bool returns a pointer to a bool.
func Bool(b bool) *bool {
	return &b
}

var BoolPtr = Bool // for back-compat

// BoolDeref dereferences the bool ptr and returns it if not nil, or else
// returns def.
func BoolDeref(ptr *bool, def bool) bool {
	if ptr != nil {
		return *ptr
	}

	return def
}

var BoolPtrDerefOr = BoolDeref // for back-compat

// BoolEqual returns true if both arguments are nil or both arguments
// dereference to the same value.
func BoolEqual(a, b *bool) bool {
	if (a == nil) != (b == nil) {
		return false
	}
	if a == nil {
		return true
	}

	return *a == *b
}

// String returns a pointer to a string.
func String(s string) *string {
	return &s
}

var StringPtr = String // for back-compat

// StringDeref dereferences the string ptr and returns it if not nil, or else
// returns def.
func StringDeref(ptr *string, def string) string {
	if ptr != nil {
		return *ptr
	}

	return def
}

var StringPtrDerefOr = StringDeref // for back-compat

// StringEqual returns true if both arguments are nil or both arguments
// dereference to the same value.
func StringEqual(a, b *string) bool {
	if (a == nil) != (b == nil) {
		return false
	}
	if a == nil {
		return true
	}

	return *a == *b
}
