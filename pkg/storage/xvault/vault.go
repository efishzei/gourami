package xvault

import (
	"os"

	"bitbucket.com/efishery/gourami/config/vault"
	"bitbucket.com/efishery/gourami/pkg/xerrors"
)

// NewVaultProvider instantiates the Vault client using configuration defined in environment variables.
func NewVaultProvider() (*vault.Provider, error) {
	vaultPath := os.Getenv("VAULT_PATH")
	vaultToken := os.Getenv("VAULT_TOKEN")
	vaultAddress := os.Getenv("VAULT_ADDRESS")

	provider, err := vault.New(vaultToken, vaultAddress, vaultPath)
	if err != nil {
		return nil, xerrors.WrapErrorf(err, xerrors.ErrorCodeUnknown, "vault.New ")
	}

	return provider, nil
}
