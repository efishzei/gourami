package xgorm

import (
	"fmt"
	"log"
	"net/url"
	"time"

	"bitbucket.com/efishery/gourami/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func NewGorm(conf *config.Configuration) (*gorm.DB, error) {
	get := func(v string) string {
		res, err := conf.Get(v)
		if err != nil {
			log.Fatalf("Couldn't get configuration value for %s: %s", v, err)
		}

		return res
	}

	databaseUsername := get("DATABASE_USERNAME")
	databasePassword := get("DATABASE_PASSWORD")
	databaseHost := get("DATABASE_HOST")
	databasePort := get("DATABASE_PORT")
	databaseName := get("DATABASE_NAME")
	databaseSSLMode := get("DATABASE_SSLMODE")

	dsn := url.URL{
		Scheme: "postgres",
		User:   url.UserPassword(databaseUsername, databasePassword),
		Host:   fmt.Sprintf("%s:%s", databaseHost, databasePort),
		Path:   databaseName,
	}
	q := dsn.Query()
	q.Add("sslmode", databaseSSLMode)

	db, err := gorm.Open(postgres.Open(dsn.String()), &gorm.Config{})
	sqlDB, _ := db.DB()

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	sqlDB.SetMaxIdleConns(10)
	// SetMaxOpenConns sets the maximum number of open connections to the database.
	sqlDB.SetMaxOpenConns(20)
	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	sqlDB.SetConnMaxLifetime(time.Hour)

	return db, err
}
