package xsql

import (
	"database/sql"
	"time"
)

// ToNullString : invalidates a sql.NullString if empty, validates if not empty.
func ToNullString(s string) sql.NullString {
	return sql.NullString{String: s, Valid: s != ""}
}

// ToNullString : invalidates a sql.NullBool if empty, validates if not bool value.
func ToNullBool(v bool) sql.NullBool {
	return sql.NullBool{Bool: v, Valid: true}
}

// ToNullInt32 : invalidates a sql.NullInt32 if empty, validates if not empty.
func ToNullInt32(i int32) sql.NullInt32 {
	return sql.NullInt32{Int32: i, Valid: true}
}

// ToNullInt64 : invalidates a sql.NullInt64 if empty, validates if not empty.
func ToNullInt64(i int64) sql.NullInt64 {
	return sql.NullInt64{Int64: i, Valid: true}
}

// ToNullFloat64 : invalidates a sql.NullInt32 if empty, validates if not empty.
func ToNullFloat64(i float64) sql.NullFloat64 {
	return sql.NullFloat64{Float64: i, Valid: true}
}

// ToNullTime : invalidates a sql.NullTime if empty, validates if not empty.
func ToNullTime(i time.Time) sql.NullTime {
	return sql.NullTime{
		Time:  i,
		Valid: i.IsZero(),
	}
}
