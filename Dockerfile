# Build Stage
FROM golang:1.17 AS build-stage
WORKDIR /app
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main .

# Final Stage
FROM gcr.io/distroless/base-debian11 as production
COPY --from=build /app/main /
EXPOSE 8003

ENTRYPOINT [ "./main" ]