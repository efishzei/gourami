package main

import (
	"bitbucket.com/efishery/gourami/cmd"
)

func main() {
	cmd.Execute()
}
