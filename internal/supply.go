package supply

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/google/uuid"

	"bitbucket.com/efishery/gourami/pkg/xerrors"
)

// Type is type of supply represent our business unit for now.
type Type string

const (
	FISH   Type = "FISH"
	SHRIMP Type = "SHRIMP"
)

func (s Type) Validate() error {
	switch s {
	case FISH, SHRIMP:
		return nil
	}

	return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "unknown supply type")
}

// Category used by SHRIMP only.
type Category string

const (
	CategoryPond       Category = "KOLAM"
	CategoryBatchPanen Category = "BATCH_PANEN"
)

func (s Category) Validate() error {
	switch s {
	case CategoryPond, CategoryBatchPanen:
		return nil
	}

	return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "unknown supply category")
}

// CultivationSystem enum.
type CultivationSystem string

const (
	CultivationSystemINTENSIVE     CultivationSystem = "INTENSIVE"
	CultivationSystemSEMIINTENSIVE CultivationSystem = "SEMI INTENSIVE"
	CultivationSystemTRADISIONAL   CultivationSystem = "TRADISIONAL" //nolint: misspell
)

// HarvestType used by shrimp only.
type HarvestType string

const (
	HarvestTypeTOTAL   HarvestType = "TOTAL"
	HarvestTypePARTIAL HarvestType = "PARTIAL"
)

// PondConstruction used by shrimp only.
type PondConstruction string

const (
	PondConstructionBETON  PondConstruction = "BETON"
	PondConstructionMULSA  PondConstruction = "MULSA"
	PondConstructionHDPE   PondConstruction = "HDPE"
	PondConstructionTANAH  PondConstruction = "TANAH"
	PondConstructionTERPAL PondConstruction = "TERPAL"
)

type UnitCommoditySize string

const (
	UnitCommoditySizeGRAMEKOR UnitCommoditySize = "GRAM/EKOR"
	UnitCommoditySizeEKORKG   UnitCommoditySize = "EKOR/KG"
	UnitCommoditySizeGRAMPCS  UnitCommoditySize = "GRAM/PCS"
	UnitCommoditySizePCSPAK   UnitCommoditySize = "PCS/PAK"
)

// Supplier represent farmers add supply to our system.
// Supplier data comes from customer service by leads id.
type Supplier struct {
	SupplierID   string
	SupplierName string
	SupplierTelp string

	PostalProvinceName string
	PostalProvinceID   int64
	PostalCityName     string
	PostalCityID       int64
	PointName          string
}

func (s Supplier) Validate() error {
	if s.SupplierName == "" {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "supplier name narus diisi")
	}

	return nil
}

// Product represent data of product or SKU.
// Product data comes from freshdb service and search by skus.
type Product struct {
	ID                  string
	SKUName             string
	PackingSize         string
	CommoditySize       float32
	CommoditySizeMax    float32
	UnitCommoditySize   UnitCommoditySize
	Grade               string
	Remark              string
	UnitInventory       string
	ProductType         string
	CreatedAt           time.Time
	UpdatedAt           time.Time
	CommodityCategoryID string
	CommodityCategory   struct {
		ID            string
		Category      string
		CommodityName string
	}
	IsCanvassing bool
}

// Canvassing is activity from farmers for create new supply.
type Canvassing struct {
	// Canvassing
	ID string

	// Supplier from customer service
	SupplierID string
	Supplier   Supplier

	// SHARED: FISH & SHRIMP
	PriceOnFarm       int32
	PriceOnFarmMax    *int32
	PriceOnCar        *int32
	FeedConsumptionKg float32
	CommoditySize     float32
	CommoditySizeMax  float32
	UnitCommoditySize UnitCommoditySize
	SupplyType        Type
	PondCount         int32

	// Total panen / Total berat panen
	HarvestTonnageEstimationKg float32 // NOTE: Apakah ada minimal ?
	HarvestDateStart           time.Time
	HarvestDateEnd             time.Time

	IsInterest          bool
	ReasonNotInterested string
	HasHarvestTeam      *bool
	HasSortingTeam      *bool
	HasLogistic         *bool
	Notes               string

	FeedType    *string
	IsSampling  bool
	HasContract bool
	DeletedAt   time.Time
	CreatedAt   time.Time
	UpdatedAt   time.Time
	CreatedBy   string
	UpdatedBy   string
	CreatedApp  string
	UpdatedApp  string

	// Fish Only
	ProductID *string
	Product   Product

	// SHRIMP ONLY
	HarvestType    *HarvestType
	SupplyCategory *Category

	PondName             *string
	PondAddress          *string
	PondProvinceName     *string
	PondProvinceID       *int64
	PondCityName         string
	PondCityID           int64
	PondSubdistrictName  string
	PondSubdistrictID    int64
	PondConstruction     *PondConstruction
	CultivationSystem    *CultivationSystem
	CultivationDateStart time.Time
}

func (s *Canvassing) GetID() {
	if s.ID == "" {
		uid := uuid.New()
		s.ID = uid.String()
	}
}

func (s *Canvassing) SetSupplier(supplier Supplier) error {
	s.Supplier = supplier

	return s.Supplier.Validate()
}

func (s Canvassing) Validate() error {
	if err := validation.ValidateStruct(
		&s,
		validation.Field(&s.SupplierID, validation.Required),
		validation.Field(&s.SupplyType),
	); err != nil {
		return xerrors.WrapErrorf(err, xerrors.ErrorCodeInvalidArgument, "general invalid values")
	}

	if err := s.ShrimpValidate(); err != nil {
		return xerrors.WrapErrorf(err, xerrors.ErrorCodeInvalidArgument, "shrimp invalid values")
	}

	if err := s.FishValidate(); err != nil {
		return xerrors.WrapErrorf(err, xerrors.ErrorCodeInvalidArgument, "fish invalid values")
	}

	return nil
}

//nolint: gocognit, cyclop
func (s *Canvassing) ShrimpValidate() error {
	if s.SupplyType != SHRIMP {
		return nil
	}

	if err := s.SupplyCategory.Validate(); err != nil {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "harga max udang harus diisi")
	}

	if *s.PriceOnFarmMax <= 0 {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "harga max udang harus diisi")
	}

	if s.HarvestType == nil {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "jenis panen udang harus diisi")
	}

	if *s.SupplyCategory == CategoryPond && s.PondName == nil {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "nama kolam udang harus diisi")
	}

	if *s.SupplyCategory == CategoryPond && (s.PondCityName == "" || s.PondCityID <= 0) {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "alamat kota kolam udang harus diisi")
	}

	if *s.SupplyCategory == CategoryPond && (s.PondSubdistrictName == "" && s.PondSubdistrictID <= 0) {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "alamat kelurahan kolam udang harus diisi")
	}

	if *s.SupplyCategory == CategoryPond && s.PondConstruction == nil {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "konstruksi kolam udang harus diisi")
	}

	if *s.SupplyCategory == CategoryPond && s.CultivationSystem == nil {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "sistem budidaya udang harus diisi")
	}

	if *s.SupplyCategory == CategoryPond && s.CultivationDateStart.IsZero() {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "tanggal tebar udang harus diisi")
	}

	if *s.SupplyCategory == CategoryBatchPanen && s.PondCount <= 0 {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "jumlah kolam udang harus diisi")
	}

	return nil
}

func (s *Canvassing) FishValidate() error {
	if s.SupplyType != FISH {
		return nil
	}

	if s.ProductID == nil {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "produk SKU harus diisi")
	}

	if s.PondCount <= 0 {
		return xerrors.NewErrorf(xerrors.ErrorCodeInvalidArgument, "jumlah kolam harus diisi")
	}

	return nil
}
