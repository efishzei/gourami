package productsvc

import (
	"context"
	"time"

	supply "bitbucket.com/efishery/gourami/internal"
)

type ProductService struct {
	URL   string
	Token string
}

func NewProductService(url, token string) *ProductService {
	return &ProductService{URL: url, Token: token}
}

func (p *ProductService) GetProductBySKU(ctx context.Context, sku string) (supply.Product, error) {
	return supply.Product{
		ID:                  "6waoHudXTL47jnGHHwfEtg",
		SKUName:             "Gurame Fresh Hidup - 800-1000 gram/ekor",
		PackingSize:         "CUSTOM_PACK_BY_WEIGHT_ORDER",
		CommoditySize:       800,
		CommoditySizeMax:    1000,
		UnitCommoditySize:   "EKOR/KG",
		Grade:               "NON_GRADE",
		Remark:              "NON",
		UnitInventory:       "KG",
		ProductType:         "FINISHED_GOOD",
		CreatedAt:           time.Now(),
		UpdatedAt:           time.Now(),
		CommodityCategoryID: "2.0_COM_00027",
		CommodityCategory: struct {
			ID            string
			Category      string
			CommodityName string
		}{
			ID:            "2.0_COM_00027",
			Category:      "HIDUP",
			CommodityName: "GURAME",
		},
		IsCanvassing: true,
	}, nil
}
