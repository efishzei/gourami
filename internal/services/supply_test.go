package services

import (
	"context"
	"errors"
	"testing"
	"time"

	supply "bitbucket.com/efishery/gourami/internal"
	"bitbucket.com/efishery/gourami/internal/services/mocks"
	"bitbucket.com/efishery/gourami/pkg/utils/pointer"
	"github.com/golang/mock/gomock"
)

//nolint: paralleltest, containedctx, dupl
func TestSupply_Create(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repos := mocks.NewMockRepository(ctrl)
	repos.EXPECT().SaveCanvassing(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

	customers := mocks.NewMockCustomerService(ctrl)
	customers.EXPECT().GetSupplier(gomock.Any(), gomock.Any()).Return(supply.Supplier{
		SupplierID:         "511248253370",
		SupplierName:       "Pak Haji Jaja",
		SupplierTelp:       "08192729383",
		PostalProvinceName: "Bandung",
		PostalProvinceID:   10,
		PostalCityName:     "14045",
		PostalCityID:       2,
		PointName:          "Mang Jaja Point",
	}, nil).AnyTimes()

	type fields struct {
		repo     Repository
		customer CustomerService
		product  ProductService
	}
	type args struct {
		ctx   context.Context
		input supply.Canvassing
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "create fish supply",
			fields: fields{
				repo:     repos,
				customer: customers,
			},
			args: args{
				ctx: context.Background(),
				input: supply.Canvassing{
					SupplierID:                 "511248253370",
					ProductID:                  pointer.String("6hXXZ23UxuPFH32nCW2jgc"),
					PriceOnFarm:                30000,
					PriceOnCar:                 pointer.Int32(40000),
					FeedConsumptionKg:          20.0,
					CommoditySize:              1.0,
					CommoditySizeMax:           1.0,
					UnitCommoditySize:          "GRAM/EKOR",
					SupplyType:                 supply.FISH,
					PondCount:                  1,
					HarvestTonnageEstimationKg: 40.0,
					HarvestDateStart:           time.Time{}, // 2019-11-21
					HarvestDateEnd:             time.Time{}, // 2020-11-21
					IsInterest:                 true,
					HasHarvestTeam:             pointer.Bool(true),
					HasSortingTeam:             pointer.Bool(true),
					HasLogistic:                pointer.Bool(true),
					Notes:                      "",
					IsSampling:                 false,
					HasContract:                false,
					CultivationSystem:          (*supply.CultivationSystem)(pointer.String(string(supply.CultivationSystemINTENSIVE))),
				},
			},
			wantErr: false,
		},
		{
			name: "customer service error",
			fields: fields{
				repo:     repos,
				customer: customers,
			},
			args: args{
				ctx: context.Background(),
				input: supply.Canvassing{
					SupplierID:                 "511248253370",
					ProductID:                  pointer.String("6hXXZ23UxuPFH32nCW2jgc"),
					SupplyType:                 supply.SHRIMP,
					PriceOnFarm:                30000,
					PriceOnFarmMax:             pointer.Int32(50000),
					HarvestTonnageEstimationKg: 40.0,
					FeedConsumptionKg:          20.0,
					CommoditySize:              1.0,
					CommoditySizeMax:           1.0,
					HarvestDateStart:           time.Time{}, // 2019-11-21
					HarvestDateEnd:             time.Time{}, // 2020-11-21
					UnitCommoditySize:          "GRAM/EKOR",
					IsInterest:                 true,
					HasHarvestTeam:             pointer.Bool(true),
					HasSortingTeam:             pointer.Bool(true),
					HasLogistic:                pointer.Bool(true),
					Notes:                      "",
					CultivationSystem:          (*supply.CultivationSystem)(pointer.String(string(supply.CultivationSystemINTENSIVE))),
					HarvestType:                (*supply.HarvestType)(pointer.String(string(supply.HarvestTypeTOTAL))),
					SupplyCategory:             (*supply.Category)(pointer.String(string(supply.CategoryPond))),
					PondCount:                  1,
					PondName:                   pointer.String("Mang Jaja Kolam 1"),
					PondCityID:                 2,
					PondCityName:               "Bandung",
					PondSubdistrictID:          2,
					PondSubdistrictName:        "Antapani",
					PondConstruction:           (*supply.PondConstruction)(pointer.String(string(supply.PondConstructionBETON))),
					CultivationDateStart:       time.Now(),
				},
			},
			wantErr: false,
		},
		{
			name: "create shrimp supply",
			fields: fields{
				repo:     repos,
				customer: customerServiceFailed(ctrl),
			},
			args: args{
				ctx: context.Background(),
				input: supply.Canvassing{
					SupplierID:                 "511248253370",
					ProductID:                  pointer.String("6hXXZ23UxuPFH32nCW2jgc"),
					SupplyType:                 supply.SHRIMP,
					PriceOnFarm:                30000,
					PriceOnCar:                 pointer.Int32(40000),
					HarvestTonnageEstimationKg: 40.0,
					FeedConsumptionKg:          20.0,
					CommoditySize:              1.0,
					CommoditySizeMax:           1.0,
					HarvestDateStart:           time.Time{}, // 2019-11-21
					HarvestDateEnd:             time.Time{}, // 2020-11-21
					UnitCommoditySize:          "GRAM/EKOR",
					IsInterest:                 true,
					HasHarvestTeam:             pointer.Bool(true),
					HasSortingTeam:             pointer.Bool(true),
					HasLogistic:                pointer.Bool(true),
					Notes:                      "",
					CultivationSystem:          (*supply.CultivationSystem)(pointer.String(string(supply.CultivationSystemINTENSIVE))),
					HarvestType:                (*supply.HarvestType)(pointer.String(string(supply.HarvestTypeTOTAL))),
					SupplyCategory:             (*supply.Category)(pointer.String(string(supply.CategoryPond))),
					PondCount:                  1,
					PondName:                   pointer.String("Mang Jaja Kolam 1"),
					PondCityID:                 2,
					PondCityName:               "Bandung",
					PondSubdistrictID:          2,
					PondSubdistrictName:        "Antapani",
					PondConstruction:           (*supply.PondConstruction)(pointer.String(string(supply.PondConstructionBETON))),
					CultivationDateStart:       time.Now(),
				},
			},
			wantErr: true,
		},
		{
			name: "repository error",
			fields: fields{
				repo:     reporitoryFailed(ctrl),
				customer: customers,
			},
			args: args{
				ctx: context.Background(),
				input: supply.Canvassing{
					SupplierID:                 "511248253370",
					ProductID:                  pointer.String("6hXXZ23UxuPFH32nCW2jgc"),
					SupplyType:                 supply.SHRIMP,
					PriceOnFarm:                30000,
					PriceOnFarmMax:             pointer.Int32(50000),
					HarvestTonnageEstimationKg: 40.0,
					FeedConsumptionKg:          20.0,
					CommoditySize:              1.0,
					CommoditySizeMax:           1.0,
					HarvestDateStart:           time.Time{}, // 2019-11-21
					HarvestDateEnd:             time.Time{}, // 2020-11-21
					UnitCommoditySize:          "GRAM/EKOR",
					IsInterest:                 true,
					HasHarvestTeam:             pointer.Bool(true),
					HasSortingTeam:             pointer.Bool(true),
					HasLogistic:                pointer.Bool(true),
					Notes:                      "",
					CultivationSystem:          (*supply.CultivationSystem)(pointer.String(string(supply.CultivationSystemINTENSIVE))),
					HarvestType:                (*supply.HarvestType)(pointer.String(string(supply.HarvestTypeTOTAL))),
					SupplyCategory:             (*supply.Category)(pointer.String(string(supply.CategoryPond))),
					PondCount:                  1,
					PondName:                   pointer.String("Mang Jaja Kolam 1"),
					PondCityID:                 2,
					PondCityName:               "Bandung",
					PondSubdistrictID:          2,
					PondSubdistrictName:        "Antapani",
					PondConstruction:           (*supply.PondConstruction)(pointer.String(string(supply.PondConstructionBETON))),
					CultivationDateStart:       time.Now(),
				},
			},
			wantErr: true,
		},
		{
			name: "validate error",
			fields: fields{
				repo:     repos,
				customer: customers,
			},
			args: args{
				ctx:   context.Background(),
				input: supply.Canvassing{},
			},
			wantErr: true,
		},
		{
			name: "create fish supply; customer service error",
			fields: fields{
				repo:     repos,
				customer: customerServiceInvalidData(ctrl),
			},
			args: args{
				ctx: context.Background(),
				input: supply.Canvassing{
					SupplierID:                 "511248253370",
					ProductID:                  pointer.String("6hXXZ23UxuPFH32nCW2jgc"),
					PriceOnFarm:                30000,
					PriceOnCar:                 pointer.Int32(40000),
					FeedConsumptionKg:          20.0,
					CommoditySize:              1.0,
					CommoditySizeMax:           1.0,
					UnitCommoditySize:          "GRAM/EKOR",
					SupplyType:                 supply.FISH,
					PondCount:                  1,
					HarvestTonnageEstimationKg: 40.0,
					HarvestDateStart:           time.Time{}, // 2019-11-21
					HarvestDateEnd:             time.Time{}, // 2020-11-21
					IsInterest:                 true,
					HasHarvestTeam:             pointer.Bool(true),
					HasSortingTeam:             pointer.Bool(true),
					HasLogistic:                pointer.Bool(true),
					Notes:                      "",
					IsSampling:                 false,
					HasContract:                false,
					CultivationSystem:          (*supply.CultivationSystem)(pointer.String(string(supply.CultivationSystemINTENSIVE))),
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewSupply(
				tt.fields.repo,
				tt.fields.customer,
				tt.fields.product,
			)
			if _, err := s.Create(tt.args.ctx, tt.args.input); (err != nil) != tt.wantErr {
				t.Errorf("Supply.Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func customerServiceFailed(ctrl *gomock.Controller) *mocks.MockCustomerService {
	customers := mocks.NewMockCustomerService(ctrl)
	customers.EXPECT().GetSupplier(gomock.Any(), gomock.Any()).Return(supply.Supplier{}, errors.New("something wrong"))

	return customers
}

func customerServiceInvalidData(ctrl *gomock.Controller) *mocks.MockCustomerService {
	customers := mocks.NewMockCustomerService(ctrl)
	customers.EXPECT().GetSupplier(gomock.Any(), gomock.Any()).Return(supply.Supplier{}, nil)

	return customers
}

func reporitoryFailed(ctrl *gomock.Controller) *mocks.MockRepository {
	repos := mocks.NewMockRepository(ctrl)
	repos.EXPECT().SaveCanvassing(gomock.Any(), gomock.Any()).Return(errors.New("something wrong"))

	return repos
}
