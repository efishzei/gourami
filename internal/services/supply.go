package services

import (
	"context"

	supply "bitbucket.com/efishery/gourami/internal"
)

//go:generate mockgen --package mocks --source=supply.go --destination=mocks/supply.go
type Repository interface {
	SaveCanvassing(ctx context.Context, input supply.Canvassing) error
	SearchCanvassing(ctx context.Context, keyword string, page, limit int) ([]supply.Canvassing, error)
	GetSupplyCanvassingByLeads(ctx context.Context, leads []string) ([]supply.Canvassing, error)
}

type CustomerService interface {
	GetSupplier(ctx context.Context, supplierID string) (supply.Supplier, error)
}

type ProductService interface {
	GetProductBySKU(ctx context.Context, sku string) (supply.Product, error)
}

type Supply struct {
	repo     Repository
	customer CustomerService
	product  ProductService
}

func NewSupply(repo Repository, customer CustomerService, product ProductService) *Supply {
	return &Supply{repo: repo, customer: customer, product: product}
}

func (s *Supply) GetSupplyByLeads(ctx context.Context, leads []string) ([]supply.Canvassing, error) {
	// TODO: add span for tracing

	var canvassings = make([]supply.Canvassing, 0)
	results, err := s.repo.GetSupplyCanvassingByLeads(ctx, leads)
	if err != nil {
		return canvassings, err
	}

	for _, c := range results {
		if c.ProductID != nil {
			// NOTE: we should bulking this request to one request
			product, err := s.product.GetProductBySKU(ctx, *c.ProductID)
			if err != nil {
				return canvassings, err
			}
			c.Product = product
		}
		canvassings = append(canvassings, c)
	}

	return canvassings, nil
}

func (s *Supply) Search(ctx context.Context, keyword string, page, limit int) ([]supply.Canvassing, error) {
	// TODO: add span for tracing

	var canvassings = make([]supply.Canvassing, 0)
	results, err := s.repo.SearchCanvassing(ctx, keyword, page, limit)
	if err != nil {
		return canvassings, err
	}

	for _, c := range results {
		if c.ProductID != nil {
			// NOTE: we should bulking this request to one request
			product, err := s.product.GetProductBySKU(ctx, *c.ProductID)
			if err != nil {
				return canvassings, err
			}
			c.Product = product
		}
		canvassings = append(canvassings, c)
	}

	return canvassings, nil
}

func (s *Supply) Create(ctx context.Context, input supply.Canvassing) (supply.Canvassing, error) {
	// TODO: add span for tracing

	// Get data leads
	supplier, err := s.customer.GetSupplier(ctx, input.SupplierID)
	if err != nil {
		return input, err
	}

	// Set Supplier
	if err := input.SetSupplier(supplier); err != nil {
		return input, err
	}

	// Validating supply canvasing
	if err := input.Validate(); err != nil {
		return input, err
	}

	// get supply id
	input.GetID()

	// Save data canvasing
	if err := s.repo.SaveCanvassing(ctx, input); err != nil {
		return input, err
	}

	return input, nil
}
