package rest

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/deepmap/oapi-codegen/pkg/types"
	"github.com/getkin/kin-openapi/openapi3"
	"github.com/go-chi/chi/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	"gopkg.in/yaml.v2"

	supply "bitbucket.com/efishery/gourami/internal"
	"bitbucket.com/efishery/gourami/internal/rest/gen"
	"bitbucket.com/efishery/gourami/pkg/xerrors"
	"bitbucket.com/efishery/gourami/pkg/xrender"
)

type SupplyService interface {
	Create(ctx context.Context, input supply.Canvassing) (supply.Canvassing, error)
	Search(ctx context.Context, keyword string, page, limit int) ([]supply.Canvassing, error)
	GetSupplyByLeads(ctx context.Context, leads []string) ([]supply.Canvassing, error)
}

type SupplyHandler struct {
	svc SupplyService
}

func NewSupplyHandler(svc SupplyService) *SupplyHandler {
	return &SupplyHandler{svc: svc}
}

func (s *SupplyHandler) Register(r *chi.Mux) {
	r.Post("/supply", s.create)
	r.Get("/supply", s.search)
	r.Post("/supply/{id}", s.update)
	r.Post("/supply/lead", s.getSupplyByLeads)

	// Document handlers
	r.HandleFunc("/supply-service.yaml", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Add("Content", "application/x-yaml")
		doc, _ := openapi3.NewLoader().LoadFromFile("./internal/rest/openapi3.yaml")
		data, _ := yaml.Marshal(&doc)
		_, _ = rw.Write(data)
		rw.WriteHeader(http.StatusOK)
	})

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:3333/supply-service.yaml"),
	))
}

func (s *SupplyHandler) getSupplyByLeads(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	// Parse request from client
	var request gen.GetSupplyByLeadRequest
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		errs := xerrors.NewErrorf(xerrors.ErrorCodeNotFound, err.Error())
		xrender.ErrResponse(ctx, w, err.Error(), errs)

		return
	}

	response, err := s.svc.GetSupplyByLeads(ctx, request.Leads)
	if err != nil {
		errs := xerrors.NewErrorf(xerrors.ErrorCodeNotFound, err.Error())
		xrender.ErrResponse(ctx, w, err.Error(), errs)

		return
	}

	w.Header().Add("Content-Type", "application/json")
	xrender.Response(w, gen.CanvassingArrayResponse{
		Data:    s.canvassingResponseArrayFmt(response),
		Message: "success",
		Success: true,
	}, http.StatusOK)
}

func (s *SupplyHandler) search(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	keyword := r.URL.Query().Get("keyword")
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))

	response, err := s.svc.Search(ctx, keyword, page, limit)
	if err != nil {
		errs := xerrors.NewErrorf(xerrors.ErrorCodeNotFound, err.Error())
		xrender.ErrResponse(ctx, w, err.Error(), errs)

		return
	}

	w.Header().Add("Content-Type", "application/json")
	xrender.Response(w, gen.CanvassingArrayResponse{
		Data:    s.canvassingResponseArrayFmt(response),
		Message: "success",
		Success: true,
	}, http.StatusOK)
}

func (s *SupplyHandler) canvassingResponseArrayFmt(resps []supply.Canvassing) []gen.CanvassingResponse {
	results := make([]gen.CanvassingResponse, 0)
	for i, v := range resps {
		results = append(results, gen.CanvassingResponse{
			CommoditySize:              &resps[i].CommoditySize,
			CommoditySizeMax:           &resps[i].CommoditySizeMax,
			CultivationSystem:          (*string)(v.CultivationSystem),
			FeedConsumptionKg:          &resps[i].FeedConsumptionKg,
			FeedType:                   new(string),
			HarvestDateEnd:             &types.Date{Time: v.HarvestDateEnd},
			HarvestDateStart:           &types.Date{Time: v.HarvestDateStart},
			HarvestTonnageEstimationKg: &resps[i].HarvestTonnageEstimationKg,
			HarvestType:                (*string)(v.HarvestType),
			HasHarvestTeam:             v.HasHarvestTeam,
			HasLogistic:                v.HasLogistic,
			HasSortingTeam:             v.HasSortingTeam,
			Id:                         &resps[i].ID,
			IsInterest:                 &resps[i].IsInterest,
			Notes:                      &resps[i].Notes,
			PondAddress:                v.PondAddress,
			PondCityId:                 &resps[i].PondCityID,
			PondCityName:               &resps[i].PondCityName,
			PondConstruction:           (*string)(v.PondConstruction),
			PondName:                   v.PondName,
			PondProvinceId:             v.PondProvinceID,
			PondProvinceName:           v.PondProvinceName,
			PondSubdistrictId:          &resps[i].PondSubdistrictID,
			PondSubdistrictName:        &resps[i].PondSubdistrictName,
			PriceOnCar:                 v.PriceOnCar,
			PriceOnFarm:                &resps[i].PriceOnFarm,
			PriceOnFarmMax:             v.PriceOnFarmMax,
			ProductId:                  v.ProductID,
			Products: &gen.CanvassingProduct{
				CommodityCategory: &gen.CommodityCategory{
					Category:      &resps[i].Product.CommodityCategory.Category,
					CommodityName: &resps[i].Product.CommodityCategory.CommodityName,
					Id:            &resps[i].Product.CommodityCategory.ID,
				},
				CommodityCategoryId: &resps[i].Product.CommodityCategoryID,
				CommoditySize:       &resps[i].Product.CommoditySize,
				CommoditySizeMax:    &resps[i].Product.CommoditySizeMax,
				CreatedAt:           &resps[i].Product.CreatedAt,
				Glazing:             nil,
				Grade:               &resps[i].Product.Grade,
				Id:                  &resps[i].Product.ID,
				IsCanvassing:        &resps[i].Product.IsCanvassing,
				PackingSize:         &resps[i].Product.PackingSize,
				ProductType:         &resps[i].Product.ProductType,
				Remark:              &resps[i].Product.Remark,
				SkuName:             &resps[i].Product.SKUName,
				UnitCommoditySize:   (*string)(&resps[i].Product.UnitCommoditySize),
				UnitInventory:       &resps[i].Product.UnitInventory,
				UpdatedAt:           &resps[i].Product.UpdatedAt,
			},
			Sampling:          nil,
			SupplierId:        &resps[i].SupplierID,
			SupplierName:      new(string),
			SupplierTelp:      new(string),
			SupplyCategory:    (*string)(resps[i].SupplyCategory),
			SupplyType:        (*string)(&resps[i].SupplyType),
			UnitCommoditySize: (*string)(&resps[i].UnitCommoditySize),
		})
	}

	return results
}

func (s *SupplyHandler) create(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	// Parse request from client
	var request gen.CanvassingRequest
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		errs := xerrors.NewErrorf(xerrors.ErrorCodeNotFound, err.Error())
		xrender.ErrResponse(ctx, w, err.Error(), errs)

		return
	}

	// Request to create
	supplies := s.canvassingRequestFmt("", request)
	response, err := s.svc.Create(ctx, supplies)
	if err != nil {
		xrender.ErrResponse(ctx, w, "error create supply", err)

		return
	}

	w.Header().Add("Content-Type", "application/json")
	xrender.Response(w, gen.CanvassingSingleResponse{
		Data:    s.canvassingResponseFmt(response),
		Message: "success",
		Success: true,
	}, http.StatusOK)
}

func (s *SupplyHandler) update(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	id := chi.URLParam(r, "id")

	// Parse request from client
	var request gen.CanvassingRequest
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		errs := xerrors.NewErrorf(xerrors.ErrorCodeNotFound, err.Error())
		xrender.ErrResponse(ctx, w, err.Error(), errs)

		return
	}

	// Request to create
	supplies := s.canvassingRequestFmt(id, request)
	response, err := s.svc.Create(ctx, supplies)
	if err != nil {
		xrender.ErrResponse(ctx, w, "error create supply", err)

		return
	}

	w.Header().Add("Content-Type", "application/json")
	xrender.Response(w, gen.CanvassingSingleResponse{
		Data:    s.canvassingResponseFmt(response),
		Message: "success",
		Success: true,
	}, http.StatusOK)
}

func (s *SupplyHandler) canvassingRequestFmt(id string, request gen.CanvassingRequest) supply.Canvassing {
	return supply.Canvassing{
		ID:                         id,
		SupplierID:                 request.SupplierId,
		ProductID:                  request.ProductId,
		PriceOnFarm:                request.PriceOnFarm,
		PriceOnFarmMax:             request.PriceOnFarmMax,
		PriceOnCar:                 request.PriceOnCar,
		FeedConsumptionKg:          request.FeedConsumptionKg,
		CommoditySize:              request.CommoditySize,
		CommoditySizeMax:           *request.CommoditySizeMax,
		UnitCommoditySize:          supply.UnitCommoditySize(request.UnitCommoditySize),
		SupplyType:                 supply.Type(request.SupplyType),
		PondCount:                  int32(request.PondCount),
		HarvestTonnageEstimationKg: request.HarvestTonnageEstimationKg,
		HarvestDateStart:           request.HarvestDateEnd.Time,
		HarvestDateEnd:             request.HarvestDateEnd.Time,
		IsInterest:                 *request.IsInterest,
		HasHarvestTeam:             request.HasHarvestTeam,
		HasSortingTeam:             request.HasSortingTeam,
		HasLogistic:                request.HasLogistic,
		Notes:                      *request.Notes,
		FeedType:                   request.FeedType,
		// IsSampling:                 false,
		// HasContract:          false,
		// DeletedAt:            time.Time{},
		// CreatedAt:            time.Time{},
		// UpdatedAt:            time.Time{},
		// CreatedBy:            "",
		// UpdatedBy:            "",
		// CreatedApp:           "",
		// UpdatedApp:           "",
		HarvestType:          (*supply.HarvestType)(request.HarvestType),
		SupplyCategory:       (*supply.Category)(request.SupplyCategory),
		PondName:             request.PondName,
		PondProvinceID:       request.PondProvinceId,
		PondProvinceName:     request.PondProvinceName,
		PondCityName:         *request.PondCityName,
		PondCityID:           *request.PondCityId,
		PondSubdistrictName:  *request.PondSubdistrictName,
		PondSubdistrictID:    *request.PondSubdistrictId,
		PondConstruction:     (*supply.PondConstruction)(request.PondConstruction),
		CultivationSystem:    (*supply.CultivationSystem)(request.CultivationSystem),
		CultivationDateStart: time.Now(),
	}
}

func (s *SupplyHandler) canvassingResponseFmt(resp supply.Canvassing) gen.CanvassingResponse {
	return gen.CanvassingResponse{
		CommoditySize:              &resp.CommoditySize,
		CommoditySizeMax:           &resp.CommoditySizeMax,
		CultivationSystem:          (*string)(resp.CultivationSystem),
		FeedConsumptionKg:          &resp.FeedConsumptionKg,
		FeedType:                   resp.FeedType,
		HarvestDateEnd:             &types.Date{Time: resp.HarvestDateEnd},
		HarvestDateStart:           &types.Date{Time: resp.HarvestDateStart},
		HarvestTonnageEstimationKg: &resp.HarvestTonnageEstimationKg,
		HarvestType:                (*string)(resp.HarvestType),
		HasHarvestTeam:             resp.HasHarvestTeam,
		HasLogistic:                resp.HasLogistic,
		HasSortingTeam:             resp.HasSortingTeam,
		Id:                         &resp.ID,
		IsInterest:                 &resp.IsInterest,
		Notes:                      &resp.Notes,
		PondAddress:                new(string),
		PondCityId:                 &resp.PondCityID,
		PondCityName:               &resp.PondCityName,
		PondConstruction:           (*string)(resp.PondConstruction),
		PondName:                   resp.PondName,
		PondProvinceId:             resp.PondProvinceID,
		PondProvinceName:           resp.PondProvinceName,
		PondSubdistrictId:          &resp.PondSubdistrictID,
		PondSubdistrictName:        &resp.PondSubdistrictName,
		PriceOnCar:                 resp.PriceOnCar,
		PriceOnFarm:                &resp.PriceOnFarm,
		PriceOnFarmMax:             resp.PriceOnFarmMax,
		ProductId:                  resp.ProductID,
		Products: &gen.CanvassingProduct{
			CommodityCategory: &gen.CommodityCategory{
				Category:      &resp.Product.CommodityCategory.Category,
				CommodityName: &resp.Product.CommodityCategory.CommodityName,
				Id:            &resp.Product.CommodityCategory.ID,
			},
			CommodityCategoryId: &resp.Product.CommodityCategoryID,
			CommoditySize:       &resp.Product.CommoditySize,
			CommoditySizeMax:    &resp.Product.CommoditySizeMax,
			CreatedAt:           &resp.Product.CreatedAt,
			Glazing:             nil,
			Grade:               &resp.Product.Grade,
			Id:                  &resp.Product.ID,
			IsCanvassing:        &resp.Product.IsCanvassing,
			PackingSize:         &resp.Product.PackingSize,
			ProductType:         &resp.Product.ProductType,
			Remark:              &resp.Product.Remark,
			SkuName:             &resp.Product.SKUName,
			UnitCommoditySize:   (*string)(&resp.Product.UnitCommoditySize),
			UnitInventory:       &resp.Product.UnitInventory,
			UpdatedAt:           &resp.Product.UpdatedAt,
		},
		Sampling:          nil,
		SupplierId:        &resp.SupplierID,
		SupplierName:      &resp.Supplier.SupplierName,
		SupplierTelp:      &resp.Supplier.SupplierTelp,
		SupplyCategory:    (*string)(resp.SupplyCategory),
		SupplyType:        (*string)(&resp.SupplyType),
		UnitCommoditySize: (*string)(&resp.UnitCommoditySize),
	}
}
