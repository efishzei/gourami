package postgresql

import (
	"context"
	"fmt"
	"time"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	supply "bitbucket.com/efishery/gourami/internal"
	"bitbucket.com/efishery/gourami/internal/postgresql/db"
	"bitbucket.com/efishery/gourami/pkg/storage/xgorm"
	"bitbucket.com/efishery/gourami/pkg/xsql"
)

type SupplyCanvassing struct {
	db *gorm.DB
}

func NewSupplyCanvassing(db *gorm.DB) *SupplyCanvassing {
	return &SupplyCanvassing{db: db}
}

func (sc *SupplyCanvassing) GetSupplyCanvassingByLeads(ctx context.Context, leads []string) ([]supply.Canvassing, error) {
	canvassings := []db.SupplyCanvassing{}
	err := sc.db.Where("supplier_id IN ?", leads).Find(&canvassings).Error
	if err != nil {
		return []supply.Canvassing{}, err
	}

	return sc.searchCanvassingResTransformer(canvassings), nil
}

func (sc *SupplyCanvassing) SearchCanvassing(ctx context.Context, keyword string, page, limit int) ([]supply.Canvassing, error) {
	canvassings := []db.SupplyCanvassing{}
	err := sc.db.Scopes(
		xgorm.Paginate(page, limit),
		scopesSearchCanvassing(keyword),
	).Find(&canvassings).Error
	if err != nil {
		return []supply.Canvassing{}, err
	}

	return sc.searchCanvassingResTransformer(canvassings), nil
}

func scopesSearchCanvassing(keyword string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if keyword == "" {
			return db.Order("updated_at desc")
		}

		return db.Where(
			"id LIKE ? OR supplier_name LIKE ? OR supplier_telp LIKE ? OR supplier_id LIKE ?",
			fmt.Sprintf("%%%s%%", keyword), fmt.Sprintf("%%%s%%", keyword),
			fmt.Sprintf("%%%s%%", keyword), fmt.Sprintf("%%%s%%", keyword),
		).Order("updated_at desc")
	}
}

func (sc *SupplyCanvassing) searchCanvassingResTransformer(canvassings []db.SupplyCanvassing) []supply.Canvassing {
	var results = make([]supply.Canvassing, 0)
	for i, v := range canvassings {
		results = append(results, supply.Canvassing{
			ID:                         v.ID,
			SupplierID:                 v.SupplierID,
			ProductID:                  v.Product,
			PriceOnFarm:                v.PriceOnFarm,
			PriceOnFarmMax:             &canvassings[i].PriceOnFarm,
			PriceOnCar:                 &canvassings[i].PriceOnCar.Int32,
			FeedConsumptionKg:          v.FeedConsumptionKg,
			CommoditySize:              v.CommoditySize,
			CommoditySizeMax:           v.CommoditySizeMax,
			UnitCommoditySize:          supply.UnitCommoditySize(v.UnitCommoditySize),
			SupplyType:                 supply.Type(v.SupplyType),
			PondCount:                  v.PondCount.Int32,
			HarvestTonnageEstimationKg: v.HarvestTonnageEstimationKg,
			HarvestDateStart:           v.HarvestDateStart,
			HarvestDateEnd:             v.HarvestDateEnd,
			IsInterest:                 v.IsInterest,
			ReasonNotInterested:        v.ReasonNotInterested.String,
			HasHarvestTeam:             &canvassings[i].HasHarvestTeam,
			HasSortingTeam:             &canvassings[i].HasSortingTeam,
			HasLogistic:                &canvassings[i].HasLogistic,
			Notes:                      v.Notes.String,
			FeedType:                   v.FeedType,
			IsSampling:                 v.IsSampling,
			HasContract:                v.HasContract,
			// DeletedAt:                  time.Time{},
			// CreatedAt:                  time.Time{},
			// UpdatedAt:                  time.Time{},
			// CreatedBy:                  "",
			// UpdatedBy:                  "",
			// CreatedApp:                 "",
			// UpdatedApp:                 "",
			HarvestType:          (*supply.HarvestType)(&v.HarvestType.String),
			SupplyCategory:       (*supply.Category)(&v.SupplyCategory.String),
			PondName:             &canvassings[i].PondCityName.String,
			PondAddress:          &canvassings[i].PondAddress.String,
			PondProvinceName:     &canvassings[i].PondProvinceName.String,
			PondProvinceID:       &canvassings[i].PondProvinceID.Int64,
			PondCityName:         v.PostalCityName.String,
			PondCityID:           v.PondCityID.Int64,
			PondSubdistrictName:  v.PondSubdistrictName.String,
			PondSubdistrictID:    v.PondSubdistrictID.Int64,
			PondConstruction:     (*supply.PondConstruction)(&v.PondConstruction.String),
			CultivationSystem:    (*supply.CultivationSystem)(v.CultivationSystem),
			CultivationDateStart: v.CultivationDateStart.Time,
		})
	}

	return results
}

func (sc *SupplyCanvassing) SaveCanvassing(ctx context.Context, input supply.Canvassing) error {
	canvassing := sc.saveCanvassingReqTransformer(ctx, input)
	if err := sc.db.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&canvassing).Error; err != nil {
		return err
	}

	return nil
}

func (sc *SupplyCanvassing) saveCanvassingReqTransformer(_ context.Context, input supply.Canvassing) db.SupplyCanvassing {
	var priceOnFarmMax int32
	if input.PriceOnFarmMax != nil {
		priceOnFarmMax = *input.PriceOnFarmMax
	}

	var priceOnCar int32
	if input.PriceOnCar != nil {
		priceOnCar = *input.PriceOnCar
	}

	var harvestType string
	if input.HarvestType != nil {
		harvestType = string(*input.HarvestType)
	}

	var supplyCategory string
	if input.SupplyCategory != nil {
		supplyCategory = string(*input.SupplyCategory)
	}

	var pondConstruction string
	if input.SupplyCategory != nil {
		pondConstruction = string(*input.PondConstruction)
	}

	var pondName string
	if input.PondName != nil {
		pondName = *input.PondName
	}

	var hasHarvestTeam bool
	if input.HasHarvestTeam != nil {
		hasHarvestTeam = *input.HasHarvestTeam
	}

	var hasSortingTeam bool
	if input.HasSortingTeam != nil {
		hasSortingTeam = *input.HasSortingTeam
	}

	var hasLogistic bool
	if input.HasLogistic != nil {
		hasLogistic = *input.HasLogistic
	}

	return db.SupplyCanvassing{
		ID:                         input.ID,
		Product:                    input.ProductID,
		HarvestTonnageEstimationKg: input.HarvestTonnageEstimationKg,
		SupplyType:                 db.SupplyType(input.SupplyType),
		PriceOnFarm:                input.PriceOnFarm,
		PriceOnFarmMax:             xsql.ToNullInt32(priceOnFarmMax),
		PriceOnCar:                 xsql.ToNullInt32(priceOnCar),
		FeedConsumptionKg:          input.FeedConsumptionKg,
		SupplierID:                 input.SupplierID,
		SupplierName:               input.Supplier.SupplierName,
		SupplierTelp:               input.Supplier.SupplierTelp,
		CommoditySize:              input.CommoditySize,
		CommoditySizeMax:           input.CommoditySizeMax,
		UnitCommoditySize:          db.UnitCommoditySize(input.UnitCommoditySize),
		HarvestDateStart:           input.HarvestDateStart,
		HarvestDateEnd:             input.HarvestDateEnd,
		CreatedBy:                  xsql.ToNullString(input.CreatedBy),
		UpdatedBy:                  xsql.ToNullString(input.UpdatedBy),
		CreatedAt:                  time.Time{},
		UpdatedAt:                  time.Time{},
		IsInterest:                 input.IsInterest,
		HasHarvestTeam:             hasHarvestTeam,
		HasSortingTeam:             hasSortingTeam,
		HasLogistic:                hasLogistic,
		HarvestType:                xsql.ToNullString(harvestType),
		Notes:                      xsql.ToNullString(input.Notes),
		DeletedAt:                  xsql.ToNullTime(input.DeletedAt),
		PostalProvinceName:         xsql.ToNullString(input.Supplier.PostalProvinceName),
		PostalCityName:             xsql.ToNullString(input.Supplier.PostalCityName),
		PointName:                  xsql.ToNullString(input.Supplier.PointName),
		SupplyCategory:             xsql.ToNullString(supplyCategory),
		CultivationSystem:          (*string)(input.CultivationSystem),
		PondConstruction:           xsql.ToNullString(pondConstruction),
		PondName:                   xsql.ToNullString(pondName),
		PondCityName:               xsql.ToNullString(input.PondCityName),
		PondCityID:                 xsql.ToNullInt64(input.PondCityID),
		PondSubdistrictName:        xsql.ToNullString(input.PondSubdistrictName),
		PondSubdistrictID:          xsql.ToNullInt64(input.PondSubdistrictID),
		PostalCityID:               xsql.ToNullInt64(input.Supplier.PostalCityID),
		PostalProvinceID:           xsql.ToNullInt64(input.Supplier.PostalProvinceID),
		PondCount:                  xsql.ToNullInt32(input.PondCount),
		CultivationDateStart:       xsql.ToNullTime(input.CultivationDateStart),
		FeedType:                   input.FeedType,
		IsSampling:                 input.IsSampling,
		HasContract:                input.HasContract,
		ReasonNotInterested:        xsql.ToNullString(input.ReasonNotInterested),
	}
}
