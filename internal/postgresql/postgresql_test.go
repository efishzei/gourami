//go:build integration
// +build integration

package postgresql

import (
	"context"
	"fmt"
	"testing"
	"time"

	"bitbucket.com/efishery/gourami/config"
	supply "bitbucket.com/efishery/gourami/internal"
	"bitbucket.com/efishery/gourami/pkg/storage/xgorm"
	"bitbucket.com/efishery/gourami/pkg/storage/xvault"
	"bitbucket.com/efishery/gourami/pkg/utils/pointer"
	"bitbucket.com/efishery/gourami/pkg/xerrors"
	"gorm.io/gorm"
)

func TestSupplyCanvassing_SaveCanvassing(t *testing.T) {
	_ = config.Load("../../.env.example")

	vault, err := xvault.NewVaultProvider()
	if err != nil {
		fmt.Println(xerrors.WrapErrorf(err, xerrors.ErrorCodeUnknown, "internal.NewVaultProvider"))
		return
	}

	conf := config.New(vault)

	db, err := xgorm.NewGorm(conf)

	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx   context.Context
		input supply.Canvassing
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "fish canvassing",
			fields: fields{
				db: db,
			},
			args: args{
				ctx: context.Background(),
				input: supply.Canvassing{
					ID:         "e92e8df7-fish-001",
					SupplierID: "511248253370",
					Supplier: supply.Supplier{
						SupplierID:         "511248253370",
						SupplierName:       "Pak Haji Jaja",
						SupplierTelp:       "08192729383",
						PostalProvinceName: "Bandung",
						PostalProvinceID:   10,
						PostalCityName:     "14045",
						PostalCityID:       2,
						PointName:          "Mang Jaja Point",
					},
					ProductID:                  pointer.String("6hXXZ23UxuPFH32nCW2jgc"),
					PriceOnFarm:                30000,
					PriceOnFarmMax:             pointer.Int32(0),
					PriceOnCar:                 pointer.Int32(4000),
					FeedConsumptionKg:          20.0,
					CommoditySize:              1.0,
					CommoditySizeMax:           1.0,
					UnitCommoditySize:          "GRAM/EKOR",
					SupplyType:                 supply.FISH,
					PondCount:                  1,
					HarvestTonnageEstimationKg: 40.0,
					HarvestDateStart:           time.Now(),
					HarvestDateEnd:             time.Now(),
					IsInterest:                 true,
					HasHarvestTeam:             pointer.Bool(true),
					HasSortingTeam:             pointer.Bool(true),
					HasLogistic:                pointer.Bool(true),
					Notes:                      "",
					HasContract:                false,
					CultivationSystem:          (*supply.CultivationSystem)(pointer.StringPtr("INTENSIVE")),
				},
			},
			wantErr: false,
		},
		{
			name: "shrimp canvassing",
			fields: fields{
				db: db,
			},
			args: args{
				ctx: context.Background(),
				input: supply.Canvassing{
					ID:         "e92e8df7-shrimp-001",
					SupplyType: supply.SHRIMP,
					SupplierID: "511248253370",
					Supplier: supply.Supplier{
						SupplierID:         "511248253370",
						SupplierName:       "Pak Haji Jaja",
						SupplierTelp:       "08192729383",
						PostalProvinceName: "Bandung",
						PostalProvinceID:   10,
						PostalCityName:     "14045",
						PostalCityID:       2,
						PointName:          "Mang Jaja Point",
					},
					ProductID:         pointer.String("6hXXZ23UxuPFH32nCW2jgc"),
					PriceOnFarm:       0,
					PriceOnFarmMax:    pointer.Int32(1000),
					CommoditySize:     1.0,
					CommoditySizeMax:  1.0,
					HarvestDateStart:  time.Now(),
					HarvestDateEnd:    time.Now(),
					UnitCommoditySize: "GRAM/EKOR",
					IsInterest:        true,
					HasHarvestTeam:    pointer.Bool(true),
					HasSortingTeam:    pointer.Bool(true),
					HasLogistic:       pointer.Bool(true),

					PondCount: 1,

					HarvestTonnageEstimationKg: 40.0,
					CultivationSystem:          (*supply.CultivationSystem)(pointer.StringPtr("INTENSIVE")),
					CultivationDateStart:       time.Now(),

					// SHRIMP Data
					PondName:            pointer.String("Kolam Pak Haji"),
					PondCityName:        "Bandung",
					PondCityID:          1,
					PondSubdistrictName: "Antapani",
					PondSubdistrictID:   2,

					HarvestType:      (*supply.HarvestType)(pointer.StringPtr(string(supply.HarvestTypeTOTAL))),
					SupplyCategory:   (*supply.Category)(pointer.StringPtr(string(supply.CategoryPond))),
					PondConstruction: (*supply.PondConstruction)(pointer.StringPtr(string(supply.PondConstructionBETON))),
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sc := &SupplyCanvassing{
				db: tt.fields.db,
			}
			if err := sc.SaveCanvassing(tt.args.ctx, tt.args.input); (err != nil) != tt.wantErr {
				t.Errorf("SupplyCanvassing.SaveCanvassing() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
