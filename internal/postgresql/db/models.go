package db

import (
	"database/sql"
	"fmt"
	"time"
)

type CultivationSystem string

const (
	CultivationSystemINTENSIVE     CultivationSystem = "INTENSIVE"
	CultivationSystemSEMIINTENSIVE CultivationSystem = "SEMI INTENSIVE"
	CultivationSystemTRADISIONAL   CultivationSystem = "TRADISIONAL" //nolint: misspell
)

func (e *CultivationSystem) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = CultivationSystem(s)
	case string:
		*e = CultivationSystem(s)
	default:
		return fmt.Errorf("unsupported scan type for CultivationSystem: %T", src)
	}

	return nil
}

type SupplyType string

const (
	SupplyTypeFISH   SupplyType = "FISH"
	SupplyTypeSHRIMP SupplyType = "SHRIMP"
)

func (e *SupplyType) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = SupplyType(s)
	case string:
		*e = SupplyType(s)
	default:
		return fmt.Errorf("unsupported scan type for SupplyType: %T", src)
	}

	return nil
}

type UnitCommoditySize string

const (
	UnitCommoditySizeGRAMEKOR UnitCommoditySize = "GRAM/EKOR"
	UnitCommoditySizeEKORKG   UnitCommoditySize = "EKOR/KG"
	UnitCommoditySizeGRAMPCS  UnitCommoditySize = "GRAM/PCS"
	UnitCommoditySizePCSPAK   UnitCommoditySize = "PCS/PAK"
)

func (e *UnitCommoditySize) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = UnitCommoditySize(s)
	case string:
		*e = UnitCommoditySize(s)
	default:
		return fmt.Errorf("unsupported scan type for UnitCommoditySize: %T", src)
	}

	return nil
}

type SupplyCanvassing struct {
	ID                         string            `json:"id"`
	Product                    *string           `json:"product"`
	HarvestTonnageEstimationKg float32           `json:"harvest_tonnage_estimation_kg"`
	SupplyType                 SupplyType        `json:"supply_type"`
	PriceOnFarm                int32             `json:"price_on_farm"`
	PriceOnFarmMax             sql.NullInt32     `json:"price_on_farm_max"`
	PriceOnCar                 sql.NullInt32     `json:"price_on_car"`
	FeedConsumptionKg          float32           `json:"feed_consumption_kg"`
	SupplierID                 string            `json:"supplier_id"`
	SupplierName               string            `json:"supplier_name"`
	SupplierTelp               string            `json:"supplier_telp"`
	CommoditySize              float32           `json:"commodity_size"`
	CommoditySizeMax           float32           `json:"commodity_size_max"`
	UnitCommoditySize          UnitCommoditySize `json:"unit_commodity_size"`
	HarvestDateStart           time.Time         `json:"harvest_date_start"`
	HarvestDateEnd             time.Time         `json:"harvest_date_end"`
	CreatedBy                  sql.NullString    `json:"created_by"`
	UpdatedBy                  sql.NullString    `json:"updated_by"`
	DeletedBy                  sql.NullString    `json:"deleted_by"`
	CreatedApp                 sql.NullString    `json:"created_app"`
	UpdatedApp                 sql.NullString    `json:"updated_app"`
	CreatedAt                  time.Time         `json:"created_at"`
	UpdatedAt                  time.Time         `json:"updated_at"`
	IsInterest                 bool              `json:"is_interest"`
	HasHarvestTeam             bool              `json:"has_harvest_team"`
	HasSortingTeam             bool              `json:"has_sorting_team"`
	HasLogistic                bool              `json:"has_logistic"`
	HarvestType                sql.NullString    `json:"harvest_type"`
	Notes                      sql.NullString    `json:"notes"`
	DeletedAt                  sql.NullTime      `json:"deleted_at"`
	PostalProvinceName         sql.NullString    `json:"postal_province_name"`
	PostalCityName             sql.NullString    `json:"postal_city_name"`
	PointName                  sql.NullString    `json:"point_name"`
	SupplyCategory             sql.NullString    `json:"supply_category"`
	CultivationSystem          *string           `json:"cultivation_system"`
	PondConstruction           sql.NullString    `json:"pond_construction"`
	PondName                   sql.NullString    `json:"pond_name"`
	PondCityName               sql.NullString    `json:"pond_city_name"`
	PondCityID                 sql.NullInt64     `json:"pond_city_id"`
	PondSubdistrictName        sql.NullString    `json:"pond_subdistrict_name"`
	PondSubdistrictID          sql.NullInt64     `json:"pond_subdistrict_id"`
	PostalCityID               sql.NullInt64     `json:"postal_city_id"`
	PostalProvinceID           sql.NullInt64     `json:"postal_province_id"`
	PondCount                  sql.NullInt32     `json:"pond_count"`
	CultivationDateStart       sql.NullTime      `json:"cultivation_date_start"`
	FeedType                   *string           `json:"feed_type"`
	IsSampling                 bool              `json:"is_sampling"`
	HasContract                bool              `json:"has_contract"`
	ReasonNotInterested        sql.NullString    `json:"reason_not_interested"`
	AssignUser                 sql.NullString    `json:"assign_user"`
	BuyerLead                  sql.NullString    `json:"buyer_lead"`
	IsContract                 bool              `json:"is_contract"`
	PondAddress                sql.NullString    `json:"pond_address"`
	PondDistrictID             sql.NullInt64     `json:"pond_district_id"`
	PondDistrictName           sql.NullString    `json:"pond_district_name"`
	PondProvinceID             sql.NullInt64     `json:"pond_province_id"`
	PondProvinceName           sql.NullString    `json:"pond_province_name"`
	Status                     sql.NullString    `json:"status"`
}

func (SupplyCanvassing) TableName() string {
	return "supply_canvassing"
}
