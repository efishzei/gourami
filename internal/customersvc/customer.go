package customersvc

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"

	supply "bitbucket.com/efishery/gourami/internal"
)

type CustomerService struct {
	URL   string
	Token string
}

func NewCustomerService(url, token string) *CustomerService {
	return &CustomerService{URL: url, Token: token}
}

func (c *CustomerService) GetSupplier(ctx context.Context, supplierID string) (supply.Supplier, error) {
	// TODO: add span for tracing

	url := fmt.Sprintf("%s/leads/%s", c.URL, supplierID)
	req, err := http.NewRequest("GET", url, bytes.NewReader(nil))
	if err != nil {
		// TODO: change to log standard
		return supply.Supplier{}, err
	}

	fmt.Println(url, c.Token)

	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("X-App-Token", c.Token)

	// TODO: inject tracing header

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		// TODO: change to log standard
		return supply.Supplier{}, err
	}
	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		// TODO: change to log standard
		return supply.Supplier{}, err
	}

	if res.StatusCode >= 300 {
		// TODO: change to log standard
		return supply.Supplier{}, errors.New(string(body))
	}

	var data GetSupplierByIDResponse
	err = json.Unmarshal(body, &data)
	if err != nil {
		// TODO: change to log standard
		fmt.Println("Error", err)
		return supply.Supplier{}, err
	}

	postalProvinceID, err := strconv.Atoi(data.Data.PostalProvinceID)
	if err != nil {
		// TODO: change to log standard
		fmt.Println("postalProvinceID", err)
		return supply.Supplier{}, err
	}

	postalCityID, err := strconv.Atoi(data.Data.PostalCityID)
	if err != nil {
		// TODO: change to log standard
		fmt.Println("postalCityID", err)
		return supply.Supplier{}, err
	}
	return supply.Supplier{
		SupplierID:         data.Data.ID,
		SupplierName:       data.Data.Name,
		SupplierTelp:       data.Data.Phone1,
		PostalProvinceName: data.Data.PostalProvinceName,
		PostalProvinceID:   int64(postalProvinceID),
		PostalCityName:     data.Data.PostalCityName,
		PostalCityID:       int64(postalCityID),
		PointName:          data.Data.Point.Name,
	}, nil
}

type GetSupplierByIDResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
	Data    struct {
		ID                 string `json:"id"`
		Name               string `json:"name"`
		Phone1             string `json:"phone1"`
		PostalAddress      string `json:"postal_address"`
		PostalProvinceName string `json:"postal_province_name"`
		PostalProvinceID   string `json:"postal_province_id"`
		PostalCityName     string `json:"postal_city_name"`
		PostalCityID       string `json:"postal_city_id"`
		Point              struct {
			Name string `json:"name"`
		} `json:"point"`
	} `json:"data"`
}
