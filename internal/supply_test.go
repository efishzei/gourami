package supply

import (
	"testing"
	"time"

	"bitbucket.com/efishery/gourami/pkg/utils/pointer"
	"github.com/stretchr/testify/assert"
)

//nolint: paralleltest, maintidx
func TestCanvassing_Validate(t *testing.T) {
	type fields struct {
		ID                         string
		SupplierID                 string
		ProductID                  string
		PriceOnFarm                int32
		PriceOnFarmMax             *int32
		PriceOnCar                 *int32
		FeedConsumptionKg          float32
		CommoditySize              float32
		CommoditySizeMax           float32
		UnitCommoditySize          UnitCommoditySize
		SupplyType                 Type
		PondCount                  int32
		HarvestTonnageEstimationKg float32
		HarvestDateStart           time.Time
		HarvestDateEnd             time.Time
		IsInterest                 bool
		ReasonNotInterested        string
		HasHarvestTeam             bool
		HasSortingTeam             bool
		HasLogistic                bool
		Notes                      string
		FeedType                   string
		IsSampling                 bool
		HasContract                bool
		DeletedAt                  time.Time
		CreatedAt                  time.Time
		UpdatedAt                  time.Time
		CreatedBy                  string
		UpdatedBy                  string
		CreatedApp                 string
		UpdatedApp                 string
		HarvestType                HarvestType
		SupplyCategory             Category
		PondName                   string
		PondCityName               string
		PondCityID                 int64
		PondSubdistrictName        string
		PondSubdistrictID          int64
		PondConstruction           PondConstruction
		CultivationSystem          CultivationSystem
		CultivationDateStart       time.Time
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "invalid supplier id blank",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            0,
				HarvestType:          "",
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(0),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid supply type",
			fields: fields{
				ID:                   "",
				SupplierID:           "",
				ProductID:            "",
				PriceOnFarm:          0,
				SupplyType:           "",
				PondCount:            0,
				HarvestType:          "",
				SupplyCategory:       "",
				PriceOnFarmMax:       pointer.Int32(0),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},

		{
			name: "invalid shrimp supply category",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            0,
				HarvestType:          "",
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(0),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; SKU empty",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            0,
				HarvestType:          "",
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(0),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; harga maximum",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            0,
				HarvestType:          "",
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(0),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; supply type",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           "",
				PondCount:            0,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; harvest type",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            0,
				HarvestType:          "",
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; supply category",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            0,
				HarvestType:          "",
				SupplyCategory:       "",
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; POND pond count",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            1,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; pond name",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            1,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "Kolam Pak Haji",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; pond city name and id",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            1,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "Kolam Pak Haji",
				PondCityName:         "Bandung",
				PondCityID:           1,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; pond subdistrict name and id",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            1,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "Kolam Pak Haji",
				PondCityName:         "Bandung",
				PondCityID:           1,
				PondSubdistrictName:  "Antapani",
				PondSubdistrictID:    2,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; pond construction",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            1,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "Kolam Pak Haji",
				PondCityName:         "Bandung",
				PondCityID:           1,
				PondSubdistrictName:  "Antapani",
				PondSubdistrictID:    2,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; pond cultivate system",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            1,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "Kolam Pak Haji",
				PondCityName:         "Bandung",
				PondCityID:           1,
				PondSubdistrictName:  "Antapani",
				PondSubdistrictID:    2,
				PondConstruction:     PondConstructionBETON,
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; cultivate date start",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            1,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "Kolam Pak Haji",
				PondCityName:         "Bandung",
				PondCityID:           1,
				PondSubdistrictName:  "Antapani",
				PondSubdistrictID:    2,
				PondConstruction:     PondConstructionBETON,
				CultivationSystem:    CultivationSystemINTENSIVE,
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid shrimp; cultivate date start",
			fields: fields{
				ID:                         "",
				SupplierID:                 "511248253370",
				ProductID:                  "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:                0,
				SupplyType:                 SHRIMP,
				PondCount:                  1,
				HarvestType:                HarvestTypeTOTAL,
				SupplyCategory:             CategoryPond,
				PriceOnFarmMax:             pointer.Int32(1000),
				HarvestTonnageEstimationKg: 0.0,
				PondName:                   "Kolam Pak Haji",
				PondCityName:               "Bandung",
				PondCityID:                 1,
				PondSubdistrictName:        "Antapani",
				PondSubdistrictID:          2,
				PondConstruction:           PondConstructionBETON,
				CultivationSystem:          CultivationSystemINTENSIVE,
				CultivationDateStart:       time.Now(),
			},
			wantErr: false,
		},
		{
			name: "invalid shrimp; BATCH_PANEN pond count",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           SHRIMP,
				PondCount:            0,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryBatchPanen,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid fish; SKU not found",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "",
				PriceOnFarm:          0,
				SupplyType:           FISH,
				PondCount:            0,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "invalid fish; pond count",
			fields: fields{
				ID:                   "",
				SupplierID:           "511248253370",
				ProductID:            "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:          0,
				SupplyType:           FISH,
				PondCount:            0,
				HarvestType:          HarvestTypeTOTAL,
				SupplyCategory:       CategoryPond,
				PriceOnFarmMax:       pointer.Int32(1000),
				PondName:             "",
				PondCityName:         "",
				PondCityID:           0.0,
				PondSubdistrictName:  "",
				PondSubdistrictID:    0.0,
				PondConstruction:     "",
				CultivationSystem:    "",
				CultivationDateStart: time.Time{},
			},
			wantErr: true,
		},
		{
			name: "success fish; request",
			fields: fields{
				SupplierID:                 "511248253370",
				ProductID:                  "6hXXZ23UxuPFH32nCW2jgc",
				PriceOnFarm:                30000,
				PriceOnCar:                 pointer.Int32(40000),
				FeedConsumptionKg:          20.0,
				CommoditySize:              1.0,
				CommoditySizeMax:           1.0,
				UnitCommoditySize:          UnitCommoditySizeGRAMEKOR,
				SupplyType:                 FISH,
				PondCount:                  1,
				HarvestTonnageEstimationKg: 40.0,
				HarvestDateStart:           time.Time{}, // 2019-11-21
				HarvestDateEnd:             time.Time{}, // 2020-11-21
				IsInterest:                 true,
				HasHarvestTeam:             true,
				HasSortingTeam:             true,
				HasLogistic:                true,
				Notes:                      "",
				IsSampling:                 false,
				HasContract:                false,
				CultivationSystem:          CultivationSystemINTENSIVE,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Canvassing{
				ID:                   tt.fields.ID,
				SupplierID:           tt.fields.SupplierID,
				ProductID:            &tt.fields.ProductID,
				PriceOnFarm:          tt.fields.PriceOnFarm,
				SupplyType:           tt.fields.SupplyType,
				PondCount:            tt.fields.PondCount,
				HarvestType:          (*HarvestType)(pointer.String(string(tt.fields.HarvestType))),
				SupplyCategory:       (*Category)(pointer.String(string(tt.fields.SupplyCategory))),
				PriceOnFarmMax:       tt.fields.PriceOnFarmMax,
				PondName:             pointer.StringPtr(tt.fields.PondName),
				PondCityName:         tt.fields.PondCityName,
				PondCityID:           tt.fields.PondCityID,
				PondSubdistrictName:  tt.fields.PondSubdistrictName,
				PondSubdistrictID:    tt.fields.PondSubdistrictID,
				PondConstruction:     (*PondConstruction)(pointer.String(string(tt.fields.PondConstruction))),
				CultivationSystem:    (*CultivationSystem)(pointer.String(string(tt.fields.CultivationSystem))),
				CultivationDateStart: tt.fields.CultivationDateStart,
			}
			if err := s.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("Canvassing.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCanvassing_SetSupplier(t *testing.T) {
	t.Parallel()

	input := Supplier{
		SupplierID:         "511248253370",
		SupplierName:       "Pak Haji Jaja",
		SupplierTelp:       "08192729383",
		PostalProvinceName: "Bandung",
		PostalProvinceID:   10,
		PostalCityName:     "14045",
		PostalCityID:       2,
		PointName:          "Mang Jaja Point",
	}

	s := &Canvassing{}
	_ = s.SetSupplier(input)

	assert.Equal(t, input, s.Supplier)
}
