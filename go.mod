module bitbucket.com/efishery/gourami

go 1.16

require (
	github.com/deepmap/oapi-codegen v1.9.1
	github.com/getkin/kin-openapi v0.89.0
	github.com/go-chi/chi/v5 v5.0.0
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/golang/mock v1.6.0
	github.com/google/uuid v1.3.0
	github.com/hashicorp/vault/api v1.3.1
	github.com/jackc/pgtype v1.9.1
	github.com/joho/godotenv v1.4.0
	github.com/spf13/cobra v1.3.0
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/http-swagger v1.2.5
	go.opentelemetry.io/otel v1.3.0
	go.uber.org/zap v1.17.0
	golang.org/x/crypto v0.0.0-20211108221036-ceb1ce70b4fa // indirect
	golang.org/x/sys v0.0.0-20220207234003-57398862261d // indirect
	google.golang.org/grpc v1.43.0 // indirect
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.5
)
