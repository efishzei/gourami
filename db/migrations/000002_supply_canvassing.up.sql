CREATE TYPE supply_type AS ENUM ('FISH', 'SHRIMP');
CREATE TYPE unit_commodity_size AS ENUM ('GRAM/EKOR','EKOR/KG', 'GRAM/PCS', 'PCS/PAK');
CREATE TYPE harvest_type AS ENUM ('TOTAL', 'PARTIAL');
CREATE TYPE supply_category AS ENUM ('KOLAM', 'BATCH PANEN');
CREATE TYPE cultivation_system AS ENUM ('INTENSIVE', 'SEMI INTENSIVE', 'TRADISIONAL');
CREATE TYPE pond_construction AS ENUM ('BETON', 'MULSA', 'HDPE', 'TANAH', 'TERPAL');

CREATE TABLE "supply_canvassing" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL,
  "product" varchar(30) COLLATE "pg_catalog"."default",
  "harvest_tonnage_estimation_kg" numeric(9,3) NOT NULL,
  "supply_type" "supply_type" NOT NULL,
  "price_on_farm" int4 NOT NULL,
  "price_on_farm_max" int4,
  "price_on_car" int4,
  "feed_consumption_kg" numeric(10,2) NOT NULL,
  "supplier_id" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "supplier_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "supplier_telp" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "commodity_size" numeric(10,2) NOT NULL,
  "commodity_size_max" numeric(10,2) NOT NULL,
  "unit_commodity_size" "unit_commodity_size" NOT NULL,
  "harvest_date_start" date NOT NULL,
  "harvest_date_end" date NOT NULL,
  "created_by" varchar(50) COLLATE "pg_catalog"."default",
  "updated_by" varchar(50) COLLATE "pg_catalog"."default",
  "deleted_by" varchar(50) COLLATE "pg_catalog"."default",
  "created_app" varchar(50) COLLATE "pg_catalog"."default",
  "updated_app" varchar(50) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6) NOT NULL,
  "updated_at" timestamptz(6) NOT NULL,
  "is_interest" bool NOT NULL,
  "has_harvest_team" bool NOT NULL,
  "has_sorting_team" bool NOT NULL,
  "has_logistic" bool NOT NULL,
  "harvest_type" "harvest_type",
  "notes" text COLLATE "pg_catalog"."default",
  "deleted_at" timestamptz(6),
  "postal_province_name" varchar(100) COLLATE "pg_catalog"."default",
  "postal_city_name" varchar(100) COLLATE "pg_catalog"."default",
  "point_name" varchar(100) COLLATE "pg_catalog"."default",
  "supply_category" "supply_category",
  "cultivation_system" "cultivation_system",
  "pond_construction" "pond_construction",
  "pond_name" varchar(100) COLLATE "pg_catalog"."default",
  "pond_city_name" varchar(100) COLLATE "pg_catalog"."default",
  "pond_city_id" int8,
  "pond_subdistrict_name" varchar(100) COLLATE "pg_catalog"."default",
  "pond_subdistrict_id" int8,
  "postal_city_id" int8,
  "postal_province_id" int8,
  "pond_count" int4,
  "cultivation_date_start" date,
  "feed_type" varchar(100) COLLATE "pg_catalog"."default",
  "is_sampling" bool NOT NULL,
  "has_contract" bool NOT NULL,
  "reason_not_interested" text COLLATE "pg_catalog"."default",
  "assign_user" varchar(100) NULL,
	"buyer_lead" varchar(30) NULL,
	"is_contract" bool NOT NULL,
	"pond_address" text NULL,
	"pond_district_id" int8 NULL,
	"pond_district_name" varchar(100) NULL,
	"pond_province_id" int8 NULL,
	"pond_province_name" varchar(100) NULL,
	"status" varchar(25) NULL
)
;
ALTER TABLE "supply_canvassing" OWNER TO "postgres";

-- ----------------------------
-- Indexes structure for table supply_canvassing
-- ----------------------------
CREATE INDEX "supply_canvassing_id_e84e78a0_like" ON "supply_canvassing" USING btree (
  "id" COLLATE "pg_catalog"."default" "pg_catalog"."varchar_pattern_ops" ASC NULLS LAST
);
CREATE INDEX "supply_canvassing_product_ebf2e810" ON "supply_canvassing" USING btree (
  "product" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "supply_canvassing_product_ebf2e810_like" ON "supply_canvassing" USING btree (
  "product" COLLATE "pg_catalog"."default" "pg_catalog"."varchar_pattern_ops" ASC NULLS LAST
);

-- ----------------------------
-- Checks structure for table supply_canvassing
-- ----------------------------
ALTER TABLE "supply_canvassing" ADD CONSTRAINT "supply_canvassing_price_on_farm_check" CHECK ((price_on_farm >= 0));
ALTER TABLE "supply_canvassing" ADD CONSTRAINT "supply_canvassing_price_on_farm_max_check" CHECK ((price_on_farm_max >= 0));
ALTER TABLE "supply_canvassing" ADD CONSTRAINT "supply_canvassing_price_on_car_check" CHECK ((price_on_car >= 0));
ALTER TABLE "supply_canvassing" ADD CONSTRAINT "supply_canvassing_pond_count_check" CHECK ((pond_count >= 0));

-- ----------------------------
-- Primary Key structure for table supply_canvassing
-- ----------------------------
ALTER TABLE "supply_canvassing" ADD CONSTRAINT "supply_canvassing_pkey" PRIMARY KEY ("id");
ALTER TABLE "supply_canvassing" ADD CONSTRAINT "supply_canvassing_status_18b89bf1_fk_enumerati" FOREIGN KEY (status) REFERENCES public.enumeration_statuscanvassing(id) DEFERRABLE INITIALLY DEFERRED;