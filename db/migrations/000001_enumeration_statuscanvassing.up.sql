CREATE TABLE public.enumeration_statuscanvassing (
	id varchar(25) NOT NULL,
	"name" varchar(25) NOT NULL,
	CONSTRAINT enumeration_statuscanvassing_pkey PRIMARY KEY (id)
);
CREATE INDEX enumeration_statuscanvassing_id_0274cfde_like ON public.enumeration_statuscanvassing (id);
