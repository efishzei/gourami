DROP TABLE IF EXISTS supply_canvassing CASCADE;
DROP TYPE IF EXISTS  supply_type; 
DROP TYPE IF EXISTS harvest_type; 
DROP TYPE IF EXISTS supply_category; 
DROP TYPE IF EXISTS cultivation_system; 
DROP TYPE IF EXISTS pond_construction; 
DROP TYPE IF EXISTS unit_commodity_size; 