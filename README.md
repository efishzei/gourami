# gourami

Supply Service as Core Service

## Installation

1. `make tools`

    Generate development tools


2. `make test`

    Make sure unit test not error. You should run before push and before change code.

3. `make lint`

    Make sure our code follow standard linter.

4. `docker-compose up -d`

    Running dependency service for development environment

5. After all dependency run then migrate

    - source env to `DB_POSTGRESQL_URL` variables, check credential for staging !WARNING
    - `migrate -database $DB_POSTGRESQL_URL -path db/migrations up`
    - make sure vpn connected

6. To make sure dependency working then test integration using `make test-db`

7. Happy Coding

## Examples

## Troubleshooting

## Changelog

## Additional resources

## License information